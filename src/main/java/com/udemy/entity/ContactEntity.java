package com.udemy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;

@Entity
@Table(name = "contacts")
public class ContactEntity {

  @Id
  @GeneratedValue
  private int id;
  private String firstName;
  private String lastName;
  private String telephone;
  private String city;

  public ContactEntity() {
    super();
  }

  public ContactEntity(int id, String firstName, String lastName, String telephone, String city) {
    super();
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.telephone = telephone;
    this.city = city;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "ContactEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
        + ", telephone=" + telephone + ", city=" + city + "]";
  }
}
