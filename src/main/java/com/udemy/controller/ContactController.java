package com.udemy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.udemy.constants.ViewConstant;
import com.udemy.model.ContactModel;

@Controller
@RequestMapping("/contact")
public class ContactController {

  @GetMapping("/contactForm")
  public String showContactForm(Model model) {
    model.addAttribute("contactModel", new ContactModel());
    return ViewConstant.CONTACT_FORM;
  }
  
  @GetMapping("/all")
  public String showAllContacts() {
    return ViewConstant.CONTACTS;
  }
}
