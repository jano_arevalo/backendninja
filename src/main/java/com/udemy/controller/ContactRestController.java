package com.udemy.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.udemy.entity.ContactEntity;
import com.udemy.repository.ContactRepository;

@RestController
@RequestMapping("/api")
public class ContactRestController {

  private static final Log logger = LogFactory.getLog(ContactRestController.class);

  @Autowired
  @Qualifier("contactRepository")
  private ContactRepository contactRepository;

  @GetMapping({"/contact"})
  public List<ContactEntity> getAllContacts() {
    return contactRepository.findAll();
  }

  @GetMapping({"/contact/{id}"})
  public ContactEntity getOneContact(@PathVariable(name = "id") int id) {
    return contactRepository.findById(id);
  }

  @PostMapping("/contact")
  public ResponseEntity<Object> createContact(@RequestBody ContactEntity entity) {
    logger.info("Entity : " + entity.toString());
    ContactEntity savedEntity = contactRepository.save(entity);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(savedEntity.getId()).toUri();
//    return ResponseEntity.created(location).build();
//    return new ResponseEntity<>(savedEntity, HttpStatus.OK);
    return ResponseEntity.created(location).body(savedEntity);
  }
  
  @PutMapping("/contact/{id}")
  public ResponseEntity<Object> updateContact(@RequestBody ContactEntity entity, @PathVariable(name = "id") int id){
    logger.info("Method : put " + "Entity : " + entity.toString());
    ContactEntity contact = contactRepository.findById(id);
    contact.setCity(entity.getCity());
    contact.setFirstName(entity.getFirstName());
    contact.setLastName(entity.getLastName());
    contact.setTelephone(entity.getTelephone());
    ContactEntity updatedEntity = contactRepository.save(contact);
    return ResponseEntity.ok().body(updatedEntity);
  }
  
  @DeleteMapping("/contact/{id}")
  public ResponseEntity<Object> deleteContact(@PathVariable(name = "id") int id){
    logger.info("Method : delete");
    contactRepository.delete(contactRepository.findById(id));
    return ResponseEntity.ok().build();
  }
}
