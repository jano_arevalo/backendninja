package com.udemy.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.udemy.constants.ViewConstant;
import com.udemy.model.UserCredential;

@Controller
public class LoginController {

  private static final Log LOG = LogFactory.getLog(LoginController.class);

  @GetMapping("/")
  public String redirectToLogin() {
    return "redirect:/login";
  }

  @GetMapping("/login")
  public String showLoginForm(@RequestParam(name = "error", required = false) String error,
      @RequestParam(name = "logout", required = false) String logout, Model model) {
    model.addAttribute("error", error);
    model.addAttribute("logout", logout);
    model.addAttribute("userCredential", new UserCredential());
    LOG.info("METHOD : showLoginForm");
    return ViewConstant.LOGIN;
  }

  @PostMapping("/loginCheck")
  public String loginCheck(@ModelAttribute(name = "userCredential") UserCredential userCredential) {
    LOG.info("METHOD : loginCheck");
    if (userCredential.getUserName().equals("user")
        && userCredential.getPassword().equals("user")) {
      LOG.info("VIEW : contacts");
      return ViewConstant.CONTACTS;
    }
    LOG.info("REDIRECT : login error");
    return "redirect:/login?error";
  }
}
